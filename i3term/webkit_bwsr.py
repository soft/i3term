#!/usr/bin/env python3
import sys, gi
gi.require_version('Gtk', '4.0')
gi.require_version('WebKit', '6.0')
from gi.repository import Gtk,WebKit
def start(app):
  view = WebKit.WebView()
  view.load_uri(sys.argv[1])
  win = Gtk.ApplicationWindow(application=app)
  win.set_size_request(800, 600)
  win.set_title("Barium help")
  win.set_child(view)
  win.connect("destroy", lambda args: view.try_close())
  win.present()

app = Gtk.Application(application_id='ogg.gtk.Help')
app.connect('activate', start)
app.run(None)


